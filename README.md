# Simple Dependency Injection for Java (SDI4j)

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=TonnyVos_sdi4j&metric=alert_status)](https://sonarcloud.io/dashboard?id=TonnyVos_sdi4j)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=TonnyVos_sdi4j&metric=security_rating)](https://sonarcloud.io/dashboard?id=TonnyVos_sdi4j)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=TonnyVos_sdi4j&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=TonnyVos_sdi4j)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=TonnyVos_sdi4j&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=TonnyVos_sdi4j)

[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=TonnyVos_sdi4j&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=TonnyVos_sdi4j)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=TonnyVos_sdi4j&metric=bugs)](https://sonarcloud.io/dashboard?id=TonnyVos_sdi4j)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=TonnyVos_sdi4j&metric=code_smells)](https://sonarcloud.io/dashboard?id=TonnyVos_sdi4j)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=TonnyVos_sdi4j&metric=coverage)](https://sonarcloud.io/dashboard?id=TonnyVos_sdi4j)

SDI4j is a simple compile-time dependency injection library for Java 11+. It generates code using annotation processors to be 
invoked during application startup. It is very light-weight, no wrappers, proxies or other intermediate objects, just 
plain Java objects and not connected to or dependent on any specific Java framework.

It uses the standard javax inject annotations:
- **Singleton** for marking an object as bean.
- **Inject** to inject an object.
- **Named** to add a qualifier to a bean or injection.

At this moment all beans are instantiated as singleton and stored in an injection context. In addition there is support 
for:
- Injecting beans by using the class or any interface it implements.
- Injecting a List of beans sharing an interface.
- Using qualifiers with beans that implement the same interface.
- Providing beans via a configuration file that have added to the injection context before instantiating generated code.
This can be useful when creating a framework that uses SDI4j.

Note that at this moment only constructor injection is supported.

## Quickstart

First step is to add the annotation processor to your maven compile plugin configuration and the runtime as dependency 
to your project

```
    <dependencies>
        ...
        <dependency>
            <groupId>nl.tvos.sdi4j</groupId>
            <artifactId>sdi4j-runtime</artifactId>
            <version>1.0.5</version>
        </dependency>
    </dependencies>
    ...

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    ...
                    <annotationProcessorPaths>
                        <path>
                            <groupId>nl.tvos.sdi4j</groupId>
                            <artifactId>sdi4j-processor</artifactId>
                            <version>1.0.5</version>
                        </path>
                    </annotationProcessorPaths>
                </configuration>
            </plugin>
            ...
```

Second step is to invoke the generated code, which will make sure that your beans are instantiated and added to the 
injection context.

```
    public static void main(String... args) {
        new InjectionContextHelper().execute();
    }
```

Final step is to start using the injection framework:

```
    @Singleton
    public class TestBean implements MyInterface {
        ...
    }

    @Singleton
    public class TestBeanWithInjects {

        @Inject
        public TestBeanWithInjects(MyInterface testInjectedBean) {...}
    }

```

## Guide
This library consists of two separate parts, the SDI4j runtime, which offers an Injection Context for storing and 
retrieving beans. The runtime can be used separately, but does not offer dependency injection by itself. 
The actual dependency injection is performed by using the second part, which is the annotation processor.
It generates code that instantiates all classes marked with the Singleton annotation and comply to one of the following
rules
- It has a no-args constructor (and no other constructors marked with the Inject annotation)
- It has a constructor marked with the Inject annotation and all constructor parameters are beans themselves that can
be instantiated before this class is instantiated (e.g. no circular dependencies)

Classes instantiated by SDI4j are singleton beans as these are only created once and stored in the Injection Context. 
The beans can be retrieved from the context by class name or by any of its interfaces.
### Injections
The most basic injection is by using the actual class of the bean, see below.
```
    @Singleton
    public class TestBean {
        ...
    }

    @Singleton
    public class TestBeanWithInjects {

        @Inject
        public TestBeanWithInjects(TestBean testInjectedBean) {...}
    }

```

In addition to using the class, you can also use any of its interfaces:
```
    @Singleton
    public class TestBean implements MyInterface {
        ...
    }

    @Singleton
    public class TestBeanWithInjects {

        @Inject
        public TestBeanWithInjects(MyInterface testInjectedBean) {...}
    }

```
#### Qualified Injections
It is possible to define qualifiers to distinguish between different implementations of an interface. 
```
    @Singleton
    @Named("1")
    public class TestBean implements MyInterface {
        ...
    }

    @Singleton
    @Named("2")
    public class TestBean2 implements MyInterface {
        ...
    }

    @Singleton
    public class TestBeanWithInjects {

        @Inject
        public TestBeanWithInjects(@Named("2") MyInterface testInjectedBean) {...}
    }

```
#### List Injections
In addition to inject single beans, you can also inject a list of beans. This will inject all beans matching the 
interface. The injection of lists works slightly different however. For all the above injections, all injected beans
have been instantiated before the target bean is instantiated (otherwise null values would be injected, but for lists 
this does not have to be case. Lists are instantiated lazy, which implies that during the construction of the target 
bean, the list might not contain all beans with the provided interface yet. Only after all beans have been instantiated
will the list be complete.
```
    @Singleton
    public class TestBean implements MyInterface {
        ...
    }

    @Singleton
    public class TestBean2 implements MyInterface {
        ...
    }

    @Singleton
    public class TestBeanWithInjects {

        @Inject
        public TestBeanWithInjects(List<MyInterface> testInjectedBeans) {...}
    }

```
So in the above example there is no guarantee that the list injected in the TestBeanWithInjects bean has both the 
TestBean and TestBean2 beans when injected, but at the end of the runtime initialization it will always have both.
### Injection Context
The Injection Context can also be used programmatically. It has a simple interface for storing and retrieving beans
from the context.
```
    // Retrieves the bean by class.    
    InjectionContext.current().getBean(TestBean.class);

    // Retrieves the bean by class/interface and qualifier.
    InjectionContext.current().getBean(MyInterface.class, "2");

    // Retrieves all beans with the interface.
    InjectionContext.current().getBeans(MyInterface.class);

    // Stores the bean by class and interfaces it implements.
    InjectionContext.current().storeBean(TestBean.class);

    // Stores the bean by class and interfaces it implements including a qualifier.
    InjectionContext.current().storeBean(TestBean.class, "2");
```