package nl.tvos.sdi4j.processor.model;

/**
 * Wrapper around the qualified class name as String to determine the class name and package name.
 */
public class BeanClassName {

    private final String qualifiedName;

    /**
     * Constructs the object without validations in the input.
     *
     * @param qualifiedName the full qualified class name.
     */
    public BeanClassName(String qualifiedName) {
        this.qualifiedName = qualifiedName;
    }

    /**
     * @return the qualified name (provided to the constructor).
     */
    public String getQualifiedName() {
        return qualifiedName;
    }

    /**
     * @return the class name (simple name).
     */
    public String getSimpleName() {
        return qualifiedName != null ? qualifiedName.substring(qualifiedName.lastIndexOf(".") + 1) : null;
    }

    /**
     * @return the package name (without the class name). If no '.' is present in the qualified name an empty string
     * is returned.
     */
    public String getPackageName() {
        return qualifiedName != null ? qualifiedName.substring(0, Math.max(0, qualifiedName.lastIndexOf("."))) : null;
    }

}
