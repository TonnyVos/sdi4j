package nl.tvos.sdi4j.processor;

/**
 * Exception thrown when processing the source code.
 */
public class BeanProcessingRuntimeException extends RuntimeException {

    /**
     * Constructor setting error message.
     *
     * @param message the error message.
     */
    public BeanProcessingRuntimeException(String message) {
        super(message);
    }

    /**
     * Constructor setting error message and root cause.
     * @param message the error message.
     * @param throwable the root cause.
     */
    public BeanProcessingRuntimeException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
