package nl.tvos.sdi4j.processor;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import nl.tvos.sdi4j.processor.model.BeanClassName;
import nl.tvos.sdi4j.processor.model.BeanInjection;
import nl.tvos.sdi4j.processor.model.BeanInjectionParameter;
import nl.tvos.sdi4j.runtime.InjectionContext;

import javax.annotation.processing.Filer;
import javax.lang.model.element.Modifier;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Writes the actual InjectionContextHelper class to the generated sources.
 */
public class ContextWriter {

    private static final String DEFAULT_PACKAGE = "nl.tvos.sdi4j.generated";

    private final Filer filer;

    /**
     * Constructor.
     *
     * @param filer the filer from the annotation processing environment.
     */
    ContextWriter(Filer filer) {
        this.filer = filer;
    }

    /**
     * Creates a new InjectionContextHelper source file and writes the bean injections as statements to this file.
     *
     * @param injections the list of bean injections.
     */
    public void writeInjectionContextHelper(List<BeanInjection> injections) {
        TypeSpec injectionContextHelper = TypeSpec.classBuilder("InjectionContextHelper")
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .addMethod(createExecuteMethod(injections))
                .build();

        try {
            JavaFile.builder(DEFAULT_PACKAGE, injectionContextHelper)
                    .build()
                    .writeTo(filer);
        } catch (IOException e) {
            throw new BeanProcessingRuntimeException("Unable to write SDI4j Injection Context helper!", e);
        }
    }

    /**
     * Creates the Execute method that is basically a list of statements creating all beans in the injection list.
     *
     * @param injections the list of injections.
     * @return the execute method spec.
     */
    private MethodSpec createExecuteMethod(List<BeanInjection> injections) {
        MethodSpec.Builder executeMethodBuilder = MethodSpec.methodBuilder("execute")
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .addStatement("$T appContext = $T.current()", InjectionContext.class, InjectionContext.class);

        // Map all injections to code blocks to insert into the execute method.
        injections.stream()
                .map(this::mapInjectionToCodeBlock)
                .forEach(executeMethodBuilder::addCode);

        return executeMethodBuilder.build();
    }

    /**
     * Maps an injection to a code block, which is a single statement that instantiates the bean and stores it in the
     * DI context.
     *
     * @param injection the injection to transform to a code block.
     * @return the clode block.
     */
    private CodeBlock mapInjectionToCodeBlock(BeanInjection injection) {
        String parameterString = injection.getParameters().stream()
                .map(this::createGetBeanStatement)
                .collect(Collectors.joining(", "));

        String qualifierString = injection.getQualifier() != null ? (", \"" + injection.getQualifier()) + "\"" : "";

        return CodeBlock.builder()
                .addStatement("appContext.storeBean(new $T(" + parameterString + ")" + qualifierString + ")",
                        createStatementParametersList(injection))
                .build();
    }

    /**
     * Creates the basic statement to transform a parameter to a statement to get the bean from the context.
     *
     * @param parameter the parameter to inject.
     * @return a statement to get the required bean from the context.
     */
    private String createGetBeanStatement(BeanInjectionParameter parameter) {
        String qualifierString = parameter.getQualifier() != null ? ", \"" + parameter.getQualifier() + "\"" : "";
        return "appContext.getBean" + (parameter.isList() ? "s" : "") + "($T.class" + qualifierString + ")";
    }

    /**
     * Create a list (array) of parameters in the correct order for the parameterized statement.
     *
     * @param injection the injection to create the parameter list for.
     * @return an array of parameters for the injection statement.
     */
    private Object[] createStatementParametersList(BeanInjection injection) {
        ClassName beanClass = mapBeanClassNameToClassName(injection.getBeanClassName());
        List<Object> statementParameters = new LinkedList<>(Collections.singletonList(beanClass));
        injection.getParameters().stream()
                .map(BeanInjectionParameter::getBeanClassName)
                .map(this::mapBeanClassNameToClassName)
                .forEach(statementParameters::add);
        return statementParameters.toArray();
    }

    /**
     * Maps a BeanClassName to the JavaPoet ClassName.
     *
     * @param beanClassName the BeanClassName.
     * @return the JavaPoet ClassName.
     */
    private ClassName mapBeanClassNameToClassName(BeanClassName beanClassName) {
        return ClassName.get(beanClassName.getPackageName(), beanClassName.getSimpleName());
    }

}
