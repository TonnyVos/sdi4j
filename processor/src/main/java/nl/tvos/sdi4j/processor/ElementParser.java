package nl.tvos.sdi4j.processor;

import javax.inject.Inject;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import java.util.Optional;

/**
 * Helper class for parsing elements and retrieving information.
 */
public class ElementParser {

    /**
     * Finds the Named annotation on the provided element if present and return the value of this element.
     *
     * @param element the element to parse.
     * @return the value of the Named annotation, wrapped in an Optional.
     */
    Optional<String> getNamedValue(Element element) {
        return element.getAnnotationMirrors().stream()
                .filter(am -> am.getAnnotationType().asElement().getSimpleName().contentEquals("Named"))
                .findFirst()
                .flatMap(am -> am.getElementValues().entrySet().stream()
                        .filter(entry -> entry.getKey().getSimpleName().contentEquals("value"))
                        .map(entry -> entry.getValue().getValue().toString())
                        .findFirst());
    }

    /**
     * Finds the first constructor element annotated with the Inject annotation as child of the provided element.
     *
     * @param element the element to parse.
     * @return the first relevant constructor element, wrapped in an Optional.
     */
    Optional<ExecutableElement> getAnnotatedConstructor(Element element) {
        return element.getEnclosedElements().stream()
                .filter(childElement -> childElement.getKind() == ElementKind.CONSTRUCTOR)
                .filter(childElement -> childElement.getAnnotationMirrors().stream()
                        .map(annotationMirror -> (TypeElement) annotationMirror.getAnnotationType().asElement())
                        .anyMatch(annotation -> annotation.getQualifiedName().toString().equals(Inject.class.getName())))
                .map(childElement -> (ExecutableElement) childElement)
                .findFirst();
    }

    /**
     * Finds a no-args constructor element as child of the provided element.
     *
     * @param element the element to parse.
     * @return the no-ards element, wrapped in an Optional.
     */
    Optional<ExecutableElement> getNoArgsConstructor(Element element) {
        return element.getEnclosedElements().stream()
                .filter(childElement -> childElement.getKind() == ElementKind.CONSTRUCTOR)
                .map(childElement -> (ExecutableElement) childElement)
                .filter(childElement -> childElement.getParameters().isEmpty())
                .findFirst();
    }
}
