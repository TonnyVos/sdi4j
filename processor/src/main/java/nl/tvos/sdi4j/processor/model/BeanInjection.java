package nl.tvos.sdi4j.processor.model;

import lombok.Builder;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Representation of a detected constructor injection in source code.
 * It contains the name of the bean with its dependencies (injected parameters in the constructor).
 */
@Builder
@Getter
public class BeanInjection {

    private final BeanClassName beanClassName;
    private final List<BeanClassName> beanInterfaces;
    private final String qualifier;
    private final List<BeanInjectionParameter> parameters;

    public List<BeanClassName> getAllClassNames() {
        List<BeanClassName> result = beanInterfaces != null ? new ArrayList<>(beanInterfaces) : new ArrayList<>();
        result.add(beanClassName);
        return result;
    }

    @Override
    public String toString() {
        return beanClassName.getQualifiedName() + parameters;
    }
}
