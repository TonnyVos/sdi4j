package nl.tvos.sdi4j.processor;

import nl.tvos.sdi4j.processor.model.BeanClassName;
import nl.tvos.sdi4j.processor.model.BeanInjection;
import nl.tvos.sdi4j.processor.model.BeanInjectionParameter;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Annotation processor for detecting all beans with their dependencies and creating a InjectionContextHelper class
 * that creates these beans and adds these to the context.
 */
public class BeanProcessor extends AbstractProcessor {

    private static final String ERROR_NOVALIDCONSTRUCTOR =
            "Bean %s does not contain with Inject annotated constructor or a NoArgs Constructor!";
    private final ElementParser elementParser = new ElementParser();
    private final List<BeanInjection> existingBeans = new ArrayList<>();

    private boolean hasProcessedRound = false;
    private ContextWriter contextWriter;

    /**
     * @return the supported annotation for this processor: @Singleton and @Inject.
     */
    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return new HashSet<>(Arrays.asList(Singleton.class.getName(), Inject.class.getName()));
    }

    /**
     * @return the minimal support source version: Java 11.
     */
    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.RELEASE_11;
    }

    /**
     * Initiates the annotation processor by creating a new context writer and reading the config if present.
     *
     * @param processingEnv the annotation processor environment.
     */
    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        contextWriter = new ContextWriter(processingEnv.getFiler());
        try {
            readConfig(processingEnv);
        } catch (IOException e) {
            // no config found.
            processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "No config file found.");
        }
    }

    /**
     * Performs the actual processing. Finds all classes annotated with Singleton and uses a constructor marked with
     * the Inject annotation or no-args constructor to initiate this bean.
     * To make sure beans are created in the correct order, the beans are sorted based on the dependencies of a bean
     * (constructor arguments).
     *
     * @param annotations the annotation types requested to be processed.
     * @param roundEnv environment for information about the current and prior round.
     * @return whether or not the set of annotation types are claimed by this processor (always true).
     */
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if (hasProcessedRound) {
            return true;
        }

        List<BeanInjection> beanInjections = roundEnv.getElementsAnnotatedWith(Singleton.class).stream()
                .filter(element -> element.getKind() == ElementKind.CLASS)
                .map(element -> (TypeElement) element)
                .map(this::mapToBeanInjection)
                .collect(Collectors.toList());

        // Sort the instantiation of beans to make sure dependencies are created before that are depended upon.
        List<BeanInjection> sortedInjections = sortBeansOnConstructionOrder(beanInjections);

        contextWriter.writeInjectionContextHelper(sortedInjections);
        hasProcessedRound = true;
        return true;
    }

    private void readConfig(ProcessingEnvironment processingEnv) throws IOException {
        FileObject fileObject = processingEnv.getFiler()
                .getResource( StandardLocation.CLASS_OUTPUT, "", "sdi4j_existingbeans" );
        try (Scanner scanner = new Scanner(fileObject.openInputStream())) {
            while (scanner.hasNextLine()) {
                existingBeans.add(BeanInjection.builder().beanClassName(new BeanClassName(scanner.nextLine())).build());
            }
        }
    }

    private BeanInjection mapToBeanInjection(TypeElement beanTypeElement) {
        return BeanInjection.builder()
                .beanClassName(new BeanClassName(beanTypeElement.getQualifiedName().toString()))
                .qualifier(elementParser.getNamedValue(beanTypeElement).orElse(null))
                .beanInterfaces(getBeanInterfaces(beanTypeElement))
                .parameters(getBeanConstructorParameters(beanTypeElement))
                .build();
    }

    private List<BeanClassName> getBeanInterfaces(TypeElement beanTypeElement) {
        return beanTypeElement.getInterfaces().stream()
                .map(TypeMirror::toString)
                .map(BeanClassName::new)
                .collect(Collectors.toList());
    }

    private List<BeanInjectionParameter> getBeanConstructorParameters(TypeElement beanTypeElement) {
        ExecutableElement executableElement = getValidBeanConstructor(beanTypeElement);
        return executableElement.getParameters().stream()
                .map(this::mapConstructorParameterToBeanInjectionParameter)
                .collect(Collectors.toList());
    }

    private ExecutableElement getValidBeanConstructor(TypeElement beanTypeElement) {
        return elementParser.getAnnotatedConstructor(beanTypeElement)
                .orElseGet(() -> elementParser.getNoArgsConstructor(beanTypeElement)
                        .orElseThrow(() -> new BeanProcessingRuntimeException(
                                String.format(ERROR_NOVALIDCONSTRUCTOR, beanTypeElement.getQualifiedName()))));
    }

    private BeanInjectionParameter mapConstructorParameterToBeanInjectionParameter(VariableElement parameterElement) {
        DeclaredType declaredType = (DeclaredType) parameterElement.asType();

        String className = declaredType.asElement().toString();
        if (!declaredType.getTypeArguments().isEmpty()) {
            className = declaredType.getTypeArguments().get(0).toString();
        }
        return BeanInjectionParameter.builder()
                .beanClassName(new BeanClassName(className))
                .qualifier(elementParser.getNamedValue(parameterElement).orElse(null))
                .isList(!declaredType.getTypeArguments().isEmpty())
                .build();
    }

    private List<BeanInjection> sortBeansOnConstructionOrder(List<BeanInjection> input) {
        List<BeanInjection> unsorted = new ArrayList<>(input);
        List<BeanInjection> result = new LinkedList<>();
        int currentSize = Integer.MAX_VALUE;
        while (currentSize > unsorted.size()) {
            currentSize = unsorted.size();
            moveNextEligibleBeans(unsorted, result);
        }
        if (unsorted.size() > 0) {
            throw new BeanProcessingRuntimeException(String.format("Unsatisfied dependencies for %d bean(s): %s!",
                    unsorted.size(), unsorted.toString()));
        }
        return result;
    }

    private void moveNextEligibleBeans(List<BeanInjection> unsorted, List<BeanInjection> sorted) {
        List<BeanInjection> eligibleBeans = unsorted.stream()
                .filter(beanInjection -> allParametersCanBeInjected(sorted, beanInjection))
                .collect(Collectors.toList());
        sorted.addAll(eligibleBeans);
        unsorted.removeAll(eligibleBeans);
    }

    private boolean allParametersCanBeInjected(List<BeanInjection> sorted, BeanInjection beanInjection) {
        return beanInjection.getParameters().stream()
                .allMatch(parameter -> parameter.isList() ||
                        parameterIsAvailable(existingBeans, parameter) ||
                        parameterIsAvailable(sorted, parameter));
    }

    private boolean parameterIsAvailable(List<BeanInjection> availableBeans, BeanInjectionParameter parameterBean) {
        return availableBeans.stream()
                .filter(availableBean -> parameterBean.getQualifier() == null ||
                        parameterBean.getQualifier().equals(availableBean.getQualifier()))
                .flatMap(availableBean -> availableBean.getAllClassNames().stream())
                .anyMatch(beanName -> parameterBean.getBeanClassName().getQualifiedName().
                        equals(beanName.getQualifiedName()));
    }
}