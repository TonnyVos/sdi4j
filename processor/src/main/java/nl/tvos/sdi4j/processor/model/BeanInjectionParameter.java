package nl.tvos.sdi4j.processor.model;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 * Parameter of a bean injection.
 */
@Builder
@Getter
@ToString
public class BeanInjectionParameter {

    private final BeanClassName beanClassName;
    private final boolean isList;
    private final String qualifier;

}
