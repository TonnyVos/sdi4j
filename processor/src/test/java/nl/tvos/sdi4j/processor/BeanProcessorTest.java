package nl.tvos.sdi4j.processor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.processing.Filer;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.FileObject;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Set;

import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class BeanProcessorTest {

    @Test
    void getSupportedAnnotationTypes_supportsInjectAndSingleton() {
        Set<String> result = new BeanProcessor().getSupportedAnnotationTypes();

        assertEquals(Set.of(Singleton.class.getName(), Inject.class.getName()), result);
    }

    @Test
    void getSupportedSourceVersion_supportsJava11() {
        SourceVersion result = new BeanProcessor().getSupportedSourceVersion();

        assertEquals(SourceVersion.RELEASE_11, result);
    }

    @Test
    void process_noBeansShouldCreateEmptyHelper() {
        BeanProcessor subject = new BeanProcessor();
        ProcessingEnvironment processingEnvironment = mockProcessingEnvironment("sdi4j_existingbeans");
        Writer writer = createWriter(processingEnvironment);

        subject.init(processingEnvironment);
        boolean result = subject.process(emptySet(), mockRoundEnvironment(emptySet()));

        assertTrue(result);
        assertEquals(readFromFile("InjectionHelper_empty.code"), writer.toString());
    }

    @Test
    void process_noCorrectBeansShouldCreateEmptyHelper() {
        BeanProcessor subject = new BeanProcessor();
        ProcessingEnvironment processingEnvironment = mockProcessingEnvironment("sdi4j_existingbeans");
        TypeElement classA = new MockElementBuilder().build();
        Writer writer = createWriter(processingEnvironment);

        subject.init(processingEnvironment);
        boolean result = subject.process(emptySet(), mockRoundEnvironment(singleton(classA)));

        assertTrue(result);
        assertEquals(readFromFile("InjectionHelper_empty.code"), writer.toString());
    }

    @Test
    void process_beanWithoutConstructorShouldThrowError() {
        BeanProcessor subject = new BeanProcessor();
        ProcessingEnvironment processingEnvironment = mockProcessingEnvironment("sdi4j_existingbeans");
        TypeElement classA = new MockElementBuilder().name("A").build();

        subject.init(processingEnvironment);
        BeanProcessingRuntimeException result = Assertions.assertThrows(BeanProcessingRuntimeException.class, () ->
                subject.process(emptySet(), mockRoundEnvironment(singleton(classA)))
        );

        assertEquals("Bean A does not contain with Inject annotated constructor or a NoArgs Constructor!",
                result.getMessage());
    }

    @Test
    void process_singleBeanWithDefaultConstructorShouldBeFound() {
        BeanProcessor subject = new BeanProcessor();
        ProcessingEnvironment processingEnvironment = mockProcessingEnvironment("sdi4j_existingbeans");
        Writer writer = createWriter(processingEnvironment);
        TypeElement classA = new MockElementBuilder().name("A").defaultConstructor().build();

        subject.init(processingEnvironment);
        boolean result = subject.process(emptySet(), mockRoundEnvironment(singleton(classA)));

        assertTrue(result);
        assertEquals(readFromFile("InjectionHelper_noargs.code"), writer.toString());
    }

    @Test
    void process_existingBeansShouldBeFound() {
        BeanProcessor subject = new BeanProcessor();
        ProcessingEnvironment processingEnvironment = mockProcessingEnvironment("sdi4j_existingbeans");
        Writer writer = createWriter(processingEnvironment);
        TypeElement classA = new MockElementBuilder().name("A").injectedConstructor("E").build();

        subject.init(processingEnvironment);
        boolean result = subject.process(emptySet(), mockRoundEnvironment(singleton(classA)));

        assertTrue(result);
        assertEquals(readFromFile("InjectionHelper_injectedFromConfig.code"), writer.toString());
    }

    @Test
    void process_injectedBeansShouldBeFound() {
        BeanProcessor subject = new BeanProcessor();
        ProcessingEnvironment processingEnvironment = mockProcessingEnvironment("sdi4j_existingbeans");
        Writer writer = createWriter(processingEnvironment);
        TypeElement classC = new MockElementBuilder().name("C").injectedConstructor("B").build();
        TypeElement classA = new MockElementBuilder().name("A").defaultConstructor().build();
        TypeElement classB = new MockElementBuilder().name("B").injectedConstructor("A").build();

        subject.init(processingEnvironment);
        boolean result = subject.process(emptySet(), mockRoundEnvironment(Set.of(classC, classA, classB)));

        assertTrue(result);
        assertEquals(readFromFile("InjectionHelper_injected.code"), writer.toString());
    }

    @Test
    void process_injectedNamedBeansShouldBeFound() {
        BeanProcessor subject = new BeanProcessor();
        ProcessingEnvironment processingEnvironment = mockProcessingEnvironment("sdi4j_existingbeans");
        Writer writer = createWriter(processingEnvironment);
        TypeElement classA = new MockElementBuilder().name("A").qualifier("X").defaultConstructor().build();
        TypeElement classB = new MockElementBuilder().name("B").injectedConstructor("A", "X").build();

        subject.init(processingEnvironment);
        boolean result = subject.process(emptySet(), mockRoundEnvironment(Set.of(classA, classB)));

        assertTrue(result);
        assertEquals(readFromFile("InjectionHelper_injectedNamed.code"), writer.toString());
    }

    @Test
    void process_noneExistingInjectedBeansShouldThrowError() {
        BeanProcessor subject = new BeanProcessor();
        ProcessingEnvironment processingEnvironment = mockProcessingEnvironment("sdi4j_existingbeans");
        TypeElement classC = new MockElementBuilder().name("C").injectedConstructor("B").build();
        TypeElement classB = new MockElementBuilder().name("B").injectedConstructor("A").build();

        subject.init(processingEnvironment);
        BeanProcessingRuntimeException result = Assertions.assertThrows(BeanProcessingRuntimeException.class, () ->
                subject.process(emptySet(), mockRoundEnvironment(Set.of(classC, classB)))
        );

        assertTrue(result.getMessage().startsWith("Unsatisfied dependencies for 2 bean(s): "));
    }

    @Test
    void process_nonExistingInjectedNamedBeansShouldThrowError() {
        BeanProcessor subject = new BeanProcessor();
        ProcessingEnvironment processingEnvironment = mockProcessingEnvironment("sdi4j_existingbeans");
        TypeElement classC = new MockElementBuilder().name("C").injectedConstructor("B", "X").build();
        TypeElement classB = new MockElementBuilder().name("B").defaultConstructor().build();
        subject.init(processingEnvironment);
        BeanProcessingRuntimeException result = Assertions.assertThrows(BeanProcessingRuntimeException.class, () ->
                subject.process(emptySet(), mockRoundEnvironment(Set.of(classC, classB)))
        );

        assertTrue(result.getMessage().startsWith("Unsatisfied dependencies for 1 bean(s): "));
    }

    private RoundEnvironment mockRoundEnvironment(Set<Element> mockedElements) {
        RoundEnvironment roundEnvironment = mock(RoundEnvironment.class);
        doReturn(mockedElements).when(roundEnvironment).getElementsAnnotatedWith(Singleton.class);
        return roundEnvironment;
    }

    private ProcessingEnvironment mockProcessingEnvironment(String existingBeanFile) {
        try {
            ProcessingEnvironment mockProcessingEnvironment = mock(ProcessingEnvironment.class);
            Filer filer = mock(Filer.class);
            when(mockProcessingEnvironment.getFiler()).thenReturn(filer);
            FileObject fileObject = mock(FileObject.class);
            when(filer.getResource(StandardLocation.CLASS_OUTPUT, "", "sdi4j_existingbeans")).thenReturn(fileObject);
            InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(existingBeanFile);
            when(fileObject.openInputStream()).thenReturn(resourceAsStream);
            return mockProcessingEnvironment;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private Writer createWriter(ProcessingEnvironment processingEnvironment) {
        try {
            Writer writer = new StringWriter();
            JavaFileObject fileObject = mock(JavaFileObject.class);
            when(processingEnvironment.getFiler().createSourceFile(any(), any())).thenReturn(fileObject);
            when(fileObject.openWriter()).thenReturn(writer);
            return writer;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private String readFromFile(String filename) {
        InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(filename);
        try {
            return readFromInputStream(resourceAsStream);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private String readFromInputStream(InputStream inputStream) throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br
                     = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }
}
