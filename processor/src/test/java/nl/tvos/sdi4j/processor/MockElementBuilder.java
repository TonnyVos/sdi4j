package nl.tvos.sdi4j.processor;

import org.apache.commons.text.TextStringBuilder;

import javax.inject.Inject;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;

import static java.util.Collections.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.doReturn;

public class MockElementBuilder {

    private final TypeElement typeElement = mock(TypeElement.class);

    public TypeElement build() {
        return typeElement;
    }

    public MockElementBuilder name(String name) {
        when(typeElement.getKind()).thenReturn(ElementKind.CLASS);
        when(typeElement.getQualifiedName()).thenReturn(new TestName(name));
        return this;
    }

    public MockElementBuilder qualifier(String name) {
        AnnotationMirror annotationMirror = createNamedAnnotation(name);
        doReturn(singletonList(annotationMirror)).when(typeElement).getAnnotationMirrors();
        return this;
    }

    public MockElementBuilder defaultConstructor() {
        ExecutableElement constructor = mock(ExecutableElement.class);
        when(constructor.getKind()).thenReturn(ElementKind.CONSTRUCTOR);
        doReturn(emptyList()).when(constructor).getParameters();
        doReturn(singletonList(constructor)).when(typeElement).getEnclosedElements();
        return this;
    }

    public MockElementBuilder injectedConstructor(String parameterClass) {
        ExecutableElement constructor = mock(ExecutableElement.class);
        when(constructor.getKind()).thenReturn(ElementKind.CONSTRUCTOR);

        VariableElement parameter = createConstructorParameter(parameterClass);
        doReturn(singletonList(parameter)).when(constructor).getParameters();

        doReturn(singletonList(constructor)).when(typeElement).getEnclosedElements();
        addAnnotationToElement(constructor, Inject.class.getName());
        return this;
    }

    public MockElementBuilder injectedConstructor(String parameterClass, String qualifier) {
        ExecutableElement constructor = mock(ExecutableElement.class);
        when(constructor.getKind()).thenReturn(ElementKind.CONSTRUCTOR);

        VariableElement parameter = createConstructorParameter(parameterClass);
        doReturn(singletonList(parameter)).when(constructor).getParameters();
        AnnotationMirror namedAnnotation = createNamedAnnotation(qualifier);
        doReturn(singletonList(namedAnnotation)).when(parameter).getAnnotationMirrors();

        doReturn(singletonList(constructor)).when(typeElement).getEnclosedElements();
        addAnnotationToElement(constructor, Inject.class.getName());
        return this;
    }

    private VariableElement createConstructorParameter(String parameterClass) {
        VariableElement parameter = mock(VariableElement.class);
        DeclaredType typeMirror = mock(DeclaredType.class);
        when(parameter.asType()).thenReturn(typeMirror);
        Element element = mock(Element.class);
        when(typeMirror.asElement()).thenReturn(element);
        when(element.toString()).thenReturn(parameterClass);
        return parameter;
    }

    private AnnotationMirror createNamedAnnotation(String name) {
        AnnotationMirror annotationMirror = createNamedAnnotationMirror();
        ExecutableElement executableElement = mock(ExecutableElement.class);
        when(executableElement.getSimpleName()).thenReturn(new TestName("value"));
        AnnotationValue annotationValue = mock(AnnotationValue.class);
        when(annotationValue.getValue()).thenReturn(name);
        doReturn(singletonMap(executableElement, annotationValue)).when(annotationMirror).getElementValues();
        return annotationMirror;
    }

    private AnnotationMirror createNamedAnnotationMirror() {
        AnnotationMirror annotationMirror = mock(AnnotationMirror.class);
        DeclaredType declaredType = mock(DeclaredType.class);
        Element element = mock(Element.class);
        when(element.getSimpleName()).thenReturn(new TestName("Named"));
        when(declaredType.asElement()).thenReturn(element);
        when(annotationMirror.getAnnotationType()).thenReturn(declaredType);
        return annotationMirror;
    }

    private void addAnnotationToElement(ExecutableElement element, String className) {
        AnnotationMirror annotationMirror = mock(AnnotationMirror.class);
        doReturn(singletonList(annotationMirror)).when(element).getAnnotationMirrors();
        DeclaredType declaredType = mock(DeclaredType.class);
        when(annotationMirror.getAnnotationType()).thenReturn(declaredType);
        TypeElement typeElement = mock(TypeElement.class);
        when(declaredType.asElement()).thenReturn(typeElement);
        when(typeElement.getQualifiedName()).thenReturn(new TestName(className));
    }

    private static class TestName extends TextStringBuilder implements Name {

        private final String value;

        TestName(String value) {
            super(value);
            this.value = value;
        }

        @Override
        public boolean contentEquals(CharSequence cs) {
            return value.contentEquals(cs);
        }
    }
}
