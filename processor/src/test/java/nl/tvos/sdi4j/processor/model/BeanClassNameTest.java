package nl.tvos.sdi4j.processor.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class BeanClassNameTest {

    @Test
    void testActualClassValue() {
        BeanClassName subject = new BeanClassName(BeanClassName.class.getName());

        assertEquals(BeanClassName.class.getName(), subject.getQualifiedName());
        assertEquals(BeanClassName.class.getPackageName(), subject.getPackageName());
        assertEquals(BeanClassName.class.getSimpleName(), subject.getSimpleName());
    }

    @Test
    void testNullClassValue() {
        BeanClassName subject = new BeanClassName(null);

        assertNull(subject.getQualifiedName());
        assertNull(subject.getPackageName());
        assertNull(subject.getSimpleName());
    }

    @Test
    void testIncorrectClassValue() {
        BeanClassName subject = new BeanClassName("This is not a class name");

        assertEquals("This is not a class name", subject.getQualifiedName());
        assertEquals("", subject.getPackageName());
        assertEquals("This is not a class name", subject.getSimpleName());
    }
}
