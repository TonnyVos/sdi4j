package nl.tvos.sdi4j.runtime;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test for the LazyBeanListWrapper class.
 */
public class LazyBeanListWrapperTest {

    private static final Map<String, Object> SINGLEITEMMAP = Map.of("", new Object());
    private static final Object RECURRINGOBJECT = new Object();

    private static final Map<String, Object> MULTIPLEITEMMAP = new LinkedHashMap<>();
    static {
        MULTIPLEITEMMAP.put("0", RECURRINGOBJECT);
        MULTIPLEITEMMAP.put("1", new Object());
        MULTIPLEITEMMAP.put("2", RECURRINGOBJECT);
    }

    @Test
    void size_returnsActualSize() {
        assertEquals(0, new LazyBeanListWrapper<>(null).size());
        assertEquals(1, new LazyBeanListWrapper<>(SINGLEITEMMAP).size());
        assertEquals(3, new LazyBeanListWrapper<>(MULTIPLEITEMMAP).size());
    }

    @Test
    void isEmpty_returnsBooleanBasedOnContents() {
        assertTrue(new LazyBeanListWrapper<>(null).isEmpty());
        assertFalse(new LazyBeanListWrapper<>(SINGLEITEMMAP).isEmpty());
        assertFalse(new LazyBeanListWrapper<>(MULTIPLEITEMMAP).isEmpty());
    }

    @Test
    void contains_returnsWhetherListContains() {
        assertFalse(new LazyBeanListWrapper<>(null).contains(SINGLEITEMMAP.get("")));
        assertTrue(new LazyBeanListWrapper<>(SINGLEITEMMAP).contains(SINGLEITEMMAP.get("")));
        assertTrue(new LazyBeanListWrapper<>(MULTIPLEITEMMAP).contains(MULTIPLEITEMMAP.get("1")));
    }

    @Test
    void iterator_returnsIteratorOfList() {
        assertFalse(new LazyBeanListWrapper<>(null).iterator().hasNext());
        assertSame(SINGLEITEMMAP.get(""), new LazyBeanListWrapper<>(SINGLEITEMMAP).iterator().next());
        assertSame(MULTIPLEITEMMAP.get("0"), new LazyBeanListWrapper<>(MULTIPLEITEMMAP).iterator().next());
    }

    @Test
    void toArray_returnsArrayWithSameContents() {
        assertEquals(0, new LazyBeanListWrapper<>(null).toArray().length);
        assertSame(SINGLEITEMMAP.get(""), new LazyBeanListWrapper<>(SINGLEITEMMAP).toArray()[0]);
        assertSame(MULTIPLEITEMMAP.get("1"), new LazyBeanListWrapper<>(MULTIPLEITEMMAP).toArray()[1]);
    }

    @Test
    void toPredefinedArray_returnsArrayWithSameContents() {
        assertNull(new LazyBeanListWrapper<>(null).toArray(new Object[1])[0]);
        assertSame(SINGLEITEMMAP.get(""), new LazyBeanListWrapper<>(SINGLEITEMMAP).toArray(new Object[1])[0]);
        assertSame(MULTIPLEITEMMAP.get("1"), new LazyBeanListWrapper<>(MULTIPLEITEMMAP).toArray(new Object[2])[1]);
    }

    @Test
    void add_throwsUnsupportedException() {
        UnsupportedOperationException result = assertThrows(UnsupportedOperationException.class, () ->
                new LazyBeanListWrapper<>(null).add(new Object()));

        assertEquals("This list is immutable!", result.getMessage());
    }

    @Test
    void remove_throwsUnsupportedException() {
        UnsupportedOperationException result = assertThrows(UnsupportedOperationException.class, () ->
                new LazyBeanListWrapper<>(null).remove(new Object()));

        assertEquals("This list is immutable!", result.getMessage());
    }

    @Test
    void containsAll_returnsWhetherListContainsAll() {
        assertFalse(new LazyBeanListWrapper<>(null).containsAll(Arrays.asList(RECURRINGOBJECT, SINGLEITEMMAP.get(""))));
        assertTrue(new LazyBeanListWrapper<>(MULTIPLEITEMMAP).containsAll(Arrays.asList(RECURRINGOBJECT, MULTIPLEITEMMAP.get("1"))));
    }

    @Test
    void addAll_throwsUnsupportedException() {
        UnsupportedOperationException result = assertThrows(UnsupportedOperationException.class, () ->
                new LazyBeanListWrapper<>(null).addAll(Collections.singletonList(new Object())));

        assertEquals("This list is immutable!", result.getMessage());
    }

    @Test
    void addAllWithIndex_throwsUnsupportedException() {
        UnsupportedOperationException result = assertThrows(UnsupportedOperationException.class, () ->
                new LazyBeanListWrapper<>(null).addAll(0, Collections.singletonList(new Object())));

        assertEquals("This list is immutable!", result.getMessage());
    }

    @Test
    void removeAll_throwsUnsupportedException() {
        UnsupportedOperationException result = assertThrows(UnsupportedOperationException.class, () ->
                new LazyBeanListWrapper<>(null).removeAll(Collections.singletonList(new Object())));

        assertEquals("This list is immutable!", result.getMessage());
    }

    @Test
    void retainAll_throwsUnsupportedException() {
        UnsupportedOperationException result = assertThrows(UnsupportedOperationException.class, () ->
                new LazyBeanListWrapper<>(null).retainAll(Collections.singletonList(new Object())));

        assertEquals("This list is immutable!", result.getMessage());
    }

    @Test
    void clear_throwsUnsupportedException() {
        UnsupportedOperationException result = assertThrows(UnsupportedOperationException.class, () ->
                new LazyBeanListWrapper<>(null).clear());

        assertEquals("This list is immutable!", result.getMessage());
    }

    @Test
    void get_returnsElementAtPositionInList() {
        assertSame(SINGLEITEMMAP.get(""), new LazyBeanListWrapper<>(SINGLEITEMMAP).get(0));
        assertSame(MULTIPLEITEMMAP.get("1"), new LazyBeanListWrapper<>(MULTIPLEITEMMAP).get(1));
    }

    @Test
    void set_throwsUnsupportedException() {
        UnsupportedOperationException result = assertThrows(UnsupportedOperationException.class, () ->
                new LazyBeanListWrapper<>(null).set(0, new Object()));

        assertEquals("This list is immutable!", result.getMessage());
    }

    @Test
    void addAtIndex_throwsUnsupportedException() {
        UnsupportedOperationException result = assertThrows(UnsupportedOperationException.class, () ->
                new LazyBeanListWrapper<>(null).add(0, new Object()));

        assertEquals("This list is immutable!", result.getMessage());
    }

    @Test
    void removeAtIndex_throwsUnsupportedException() {
        UnsupportedOperationException result = assertThrows(UnsupportedOperationException.class, () ->
                new LazyBeanListWrapper<>(null).remove(0));

        assertEquals("This list is immutable!", result.getMessage());
    }

    @Test
    void indexOf_returnsIndexOfElement() {
        assertEquals(0, new LazyBeanListWrapper<>(SINGLEITEMMAP).indexOf(SINGLEITEMMAP.get("")));
        assertEquals(0, new LazyBeanListWrapper<>(MULTIPLEITEMMAP).indexOf(RECURRINGOBJECT));
        assertEquals(1, new LazyBeanListWrapper<>(MULTIPLEITEMMAP).indexOf(MULTIPLEITEMMAP.get("1")));
    }

    @Test
    void lastIndexOf_returnsLastIndexOfElement() {
        assertEquals(0, new LazyBeanListWrapper<>(SINGLEITEMMAP).lastIndexOf(SINGLEITEMMAP.get("")));
        assertEquals(2, new LazyBeanListWrapper<>(MULTIPLEITEMMAP).lastIndexOf(RECURRINGOBJECT));
    }

    @Test
    void listIterator_returnsIteratorOfList() {
        assertFalse(new LazyBeanListWrapper<>(null).listIterator().hasNext());
        assertSame(SINGLEITEMMAP.get(""), new LazyBeanListWrapper<>(SINGLEITEMMAP).listIterator().next());
        assertSame(MULTIPLEITEMMAP.get("0"), new LazyBeanListWrapper<>(MULTIPLEITEMMAP).listIterator().next());
    }

    @Test
    void listIteratorWithIndex_returnsIteratorOfList() {
        assertSame(SINGLEITEMMAP.get(""), new LazyBeanListWrapper<>(SINGLEITEMMAP).listIterator(0).next());
        assertSame(MULTIPLEITEMMAP.get("1"), new LazyBeanListWrapper<>(MULTIPLEITEMMAP).listIterator(1).next());
    }

    @Test
    void subList_returnsIteratorOfList() {
        List<Object> result = new LazyBeanListWrapper<>(MULTIPLEITEMMAP).subList(1, 2);
        assertEquals(1, result.size());
        assertSame(MULTIPLEITEMMAP.get("1"), result.get(0));
    }
}
