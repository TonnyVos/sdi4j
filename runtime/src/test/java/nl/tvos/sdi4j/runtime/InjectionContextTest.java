package nl.tvos.sdi4j.runtime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test for InjectionContext.
 */
class InjectionContextTest {

    @BeforeEach
    void clear() {
        InjectionContext.current().clear();
    }

    @Test
    void current_alwaysReturnsSameInstance() {
        // Given
        InjectionContext injectionContext = InjectionContext.current();

        // When
        InjectionContext result = InjectionContext.current();

        // Then
        assertSame(result, injectionContext);
    }

    @Test
    void getBean_returnStoredBean() {
        // Given
        InjectionContext injectionContext = InjectionContext.current();
        TestFunction testFunction = new TestFunction();
        injectionContext.storeBean(testFunction);

        // When -> Then
        assertSame(testFunction, injectionContext.getBean(TestFunction.class));
        assertSame(testFunction, injectionContext.getBean(Consumer.class));
    }

    @Test
    void getBean_multipleBeansReturnsNoBeans() {
        // Given
        InjectionContext injectionContext = InjectionContext.current();
        injectionContext.storeBean(new TestFunction());
        injectionContext.storeBean(new TestFunction2());

        // When -> Then
        assertNull(injectionContext.getBean(Consumer.class));
    }


    @Test
    void getBeanWithQualifier_returnStoredBeanWithQualifier() {
        // Given
        InjectionContext injectionContext = InjectionContext.current();
        TestFunction testFunction = new TestFunction();
        injectionContext.storeBean(testFunction, "TESTNAME");

        // When -> Then
        assertSame(testFunction, injectionContext.getBean(TestFunction.class, "TESTNAME"));
        assertSame(testFunction, injectionContext.getBean(Consumer.class, "TESTNAME"));
    }

    @Test
    void getBeanWithQualifier_returnStoredBeanWithoutQualifier() {
        // Given
        InjectionContext injectionContext = InjectionContext.current();
        TestFunction testFunction = new TestFunction();
        injectionContext.storeBean(testFunction);

        // When -> Then
        assertSame(testFunction, injectionContext.getBean(Consumer.class, TestFunction.class.getName()));
    }

    @Test
    void getBeans_returnStoredBean() {
        // Given
        InjectionContext injectionContext = InjectionContext.current();
        TestFunction testFunction = new TestFunction();
        injectionContext.storeBean(testFunction);

        // When -> Then
        assertSame(testFunction, injectionContext.getBeans(TestFunction.class).get(0));
        assertSame(testFunction, injectionContext.getBeans(Consumer.class).get(0));
    }

    @Test
    void getBeans_multipleBeansReturnsAllBeans() {
        // Given
        InjectionContext injectionContext = InjectionContext.current();
        injectionContext.storeBean(new TestFunction());
        injectionContext.storeBean(new TestFunction2());

        // When -> Then
        assertEquals(2, injectionContext.getBeans(Consumer.class).size());
    }

    @Test
    void getBeans_emptyListWhenNoBeans() {
        // Given
        InjectionContext injectionContext = InjectionContext.current();

        // When -> Then
        assertEquals(0, injectionContext.getBeans(Consumer.class).size());
        assertEquals(0, injectionContext.getBeans(TestFunction.class).size());
    }

    @Test
    void clear_clearsAllBeans() {
        // Given
        InjectionContext injectionContext = InjectionContext.current();
        injectionContext.storeBean(new TestFunction());

        // When
        injectionContext.clear();

        // Then
        assertNull(injectionContext.getBean(TestFunction.class));
        assertEquals(0, injectionContext.getBeans(Consumer.class).size());
    }

    private static class TestFunction implements Consumer<String> {

        @Override
        public void accept(String s) {

        }
    }

    private static class TestFunction2 implements Consumer<String> {

        @Override
        public void accept(String s) {

        }
    }
}
