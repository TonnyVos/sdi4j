package nl.tvos.sdi4j.runtime;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The Injection Context contains bean registries with all instantiated beans.
 * When injecting a list of beans it is difficult to determine whether all implementations have been instantiated before
 * injecting the list into a bean. To avoid this complexity the bean registry for the specific interface is wrapped and
 * exposed as (immutable) list. This also enables limited run-time injection possibilities as the list is backed by the
 * actual map in the injection context.
 *
 * @param <T> the type of objects in this list.
 */
class LazyBeanListWrapper<T> implements List<T> {

    private final Map<String, Object> beanRegistry;

    LazyBeanListWrapper(Map<String, Object> beanRegistry) {
        this.beanRegistry = beanRegistry != null ? beanRegistry : Collections.emptyMap();
    }

    @SuppressWarnings("unchecked")
    private List<T> getInternalList() {
        return beanRegistry.values()
                .stream()
                .map(bean -> (T) bean)
                .collect(Collectors.toList());
    }

    @Override
    public int size() {
        return beanRegistry.size();
    }

    @Override
    public boolean isEmpty() {
        return beanRegistry.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return beanRegistry.containsValue(o);
    }

    @Override
    public Iterator<T> iterator() {
        return getInternalList().iterator();
    }

    @Override
    public Object[] toArray() {
        return getInternalList().toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return getInternalList().toArray(a);
    }

    @Override
    public boolean add(T t) {
        throw new UnsupportedOperationException("This list is immutable!");
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("This list is immutable!");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return beanRegistry.values().containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        throw new UnsupportedOperationException("This list is immutable!");
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        throw new UnsupportedOperationException("This list is immutable!");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("This list is immutable!");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("This list is immutable!");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("This list is immutable!");
    }

    @Override
    public T get(int index) {
        return getInternalList().get(index);
    }

    @Override
    public T set(int index, T element) {
        throw new UnsupportedOperationException("This list is immutable!");
    }

    @Override
    public void add(int index, T element) {
        throw new UnsupportedOperationException("This list is immutable!");
    }

    @Override
    public T remove(int index) {
        throw new UnsupportedOperationException("This list is immutable!");
    }

    @Override
    public int indexOf(Object o) {
        return getInternalList().indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return getInternalList().lastIndexOf(o);
    }

    @Override
    public ListIterator<T> listIterator() {
        return getInternalList().listIterator();
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return getInternalList().listIterator(index);
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return getInternalList().subList(fromIndex, toIndex);
    }
}
