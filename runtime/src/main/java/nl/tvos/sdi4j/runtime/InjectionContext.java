package nl.tvos.sdi4j.runtime;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Arrays.asList;

/**
 * Injection Context to be used at runtime providing access to all beans.
 */
public final class InjectionContext {

    private static final InjectionContext INJECTION_CONTEXT = new InjectionContext();

    private final Map<String, Map<String, Object>> beanRegistry = new ConcurrentHashMap<>();
    private final Map<String, Map<String, Object>> interfaceRegistry = new ConcurrentHashMap<>();

    private InjectionContext() {
    }

    /**
     * @return the current injection context.
     */
    public static InjectionContext current() {
        return INJECTION_CONTEXT;
    }

    /**
     * Retrieves the bean based on the class name provided as parameter. The class name can be the implementation class
     * or one of its interfaces.
     *
     * @param classType the class of the bean to find.
     * @param <T>       the type of the class to return.
     * @return the bean.
     */
    public <T> T getBean(Class<T> classType) {
        return getBean(classType, classType.isInterface() ? null : classType.getName());
    }

    /**
     * Retrieves the bean based on the class name provided as parameter. The class name can be the implementation class
     * or one of its interfaces.
     *
     * @param classType the class of the bean to find.
     * @param qualifier the qualifier identifying the specific bean implementation requested.
     * @param <T>       the type of the class to return.
     * @return the bean.
     */
    @SuppressWarnings("unchecked")
    public <T> T getBean(Class<T> classType, String qualifier) {
        Map<String, Map<String, Object>> registryToProcess = classType.isInterface() ? interfaceRegistry : beanRegistry;
        Map<String, Object> qualifiedBeans = registryToProcess.get(classType.getName());
        if (qualifiedBeans != null) {
            if (qualifier != null) {
                return (T) qualifiedBeans.get(qualifier);
            } else if (qualifiedBeans.size() == 1) {
                return (T) qualifiedBeans.values().toArray()[0];
            }
        }
        return null;
    }

    /**
     * Retrieves all beans matching a certain class or interface.
     * If no bean was mapped yet, it inserts an empty map for this type to support lazy loading.
     *
     * @param classType the class of the bean to find.
     * @param <T>       the type of the class to return.
     * @return the list of beans.
     */
    public <T> List<T> getBeans(Class<T> classType) {
        Map<String, Map<String, Object>> registryToProcess = classType.isInterface() ? interfaceRegistry : beanRegistry;
        return new LazyBeanListWrapper<>(registryToProcess.computeIfAbsent(classType.getName(), name -> new ConcurrentHashMap<>()));
    }

    /**
     * Stores an object as bean in the injection context. It stores the bean with the qualified class name as
     * qualifier, but also all of its interfaces.
     *
     * @param object the bean to store.
     */
    public void storeBean(Object object) {
        storeBean(object, object.getClass().getName());
    }

    /**
     * Stores an object as bean in the injection context. It stores the bean with the qualified class name as
     * identifier, but also all of its interfaces.
     *
     * @param object    the bean to store.
     * @param qualifier the qualifier for this bean.
     */
    public void storeBean(Object object, String qualifier) {
        Map<String, Object> qualifiedBeans = beanRegistry.
                computeIfAbsent(object.getClass().getName(), name -> new ConcurrentHashMap<>());
        qualifiedBeans.put(qualifier, object);
        asList(object.getClass().getInterfaces()).forEach(interfaceClass ->
                addBeanToInterfaceRegistry(interfaceClass.getName(), object, qualifier)
        );
    }

    private void addBeanToInterfaceRegistry(String interfaceName, Object object, String qualifier) {
        Map<String, Object> beansMappedToInterface = interfaceRegistry.
                computeIfAbsent(interfaceName, name -> new ConcurrentHashMap<>());
        beansMappedToInterface.put(qualifier, object);
    }

    /**
     * Clears the current context.
     */
    void clear() {
        beanRegistry.clear();
        interfaceRegistry.clear();
    }

}
